/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import com.codename1.io.AccessToken;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.social.Login;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Dialog;
import com.codename1.ui.util.Resources;
import com.mycompany.entities.Catalogue;
import com.pidev.forms.Home;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 *
 * @author noon3
 */
public class Logins {

    public static interface UserData {

        public String getName();

        public String getId();

        public String getImage();

        public void fetchData(String token, Runnable callback);
    }

     public static class FacebookData implements UserData {

        String email;
        String id;

        @Override
        public String getName() {
            return email;
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public String getImage() {
            return "http://graph.facebook.com/v2.9/" + id + "/picture";
        }

        @Override
        public void fetchData(String token, Runnable callback) {
            System.out.println("in da fitch");
            ConnectionRequest req = new ConnectionRequest() {
                @Override
                protected void readResponse(InputStream input) throws IOException {
                    System.out.println("in da read");
                    JSONParser parser = new JSONParser();
                    Map<String, Object> parsed = parser.parseJSON(new InputStreamReader(input, "UTF-8"));
                    email = (String) parsed.get("email");
                    id = (String) parsed.get("id");
                    System.out.println(email);
                    System.out.println(parsed);
                }

                @Override
                protected void postResponse() {
                    callback.run();
                }
            };
            req.setPost(true);
            req.setUrl("https://graph.facebook.com/v2.9/me");
            req.addArgumentNoEncoding("access_token", token);
            req.addArgument("fields", "email");
            System.out.println("finished all para");
            NetworkManager.getInstance().addToQueue(req);
        }
    }

    private String fullName;
    private String uniqueId;
    private String imageURL;


    /**
     * token expiration is in seconds from the current time, we convert it to a
     * System.currentTimeMillis value so we can reference it in the future to
     * check expiration
     */
   public static long tokenExpirationInMillis(AccessToken token) {
        String expires = token.getExpires();
        if (expires != null && expires.length() > 0) {
            try {
                // when it will expire in seconds
                long l = Long.parseLong(expires) * 1000;
                return System.currentTimeMillis() + l;
            } catch (NumberFormatException err) {
                // ignore invalid input
            }
        }
        return -1;
    }

}
