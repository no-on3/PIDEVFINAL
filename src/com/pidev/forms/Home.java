/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pidev.forms;

import com.codename1.components.InfiniteScrollAdapter;
import com.codename1.components.MultiButton;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

/**
 * GUI builder created Dialog
 *
 * @author noon3
 */
public class Home extends com.codename1.ui.Dialog {

    private String id;
    private String username;

    public Home() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }

    public Home(String id, String username) {
        this(com.codename1.ui.util.Resources.getGlobalResources());

        this.id = id;
        this.username = username;

       // get catalogues
    }

    public Home(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

    public void showForm() {
        Form hi = new Form("Home", new BoxLayout(BoxLayout.Y_AXIS));
        Image im = FontImage.createMaterial(FontImage.MATERIAL_SELECT_ALL, UIManager.getInstance().getComponentStyle("Command"));

        im = FontImage.createMaterial(FontImage.MATERIAL_SELECT_ALL, UIManager.getInstance().getComponentStyle("Command"));
        Style s = UIManager.getInstance().getComponentStyle("MultiLine1");
        FontImage p = FontImage.createMaterial(FontImage.MATERIAL_PORTRAIT, s);
        EncodedImage placeholder = EncodedImage.createFromImage(p.scaled(p.getWidth() * 3, p.getHeight() * 3), false);

        Command show = new Command("Show Mine", im) {

            @Override
            public void actionPerformed(ActionEvent evt) {
            
//                    ConnectionRequest r = new ConnectionRequest();
//                    r.setPost(false);
//                    r.setUrl("http://127.0.0.1:8000/catalogues/" + id);
//
//                    NetworkManager.getInstance().addToQueueAndWait(r);
//                    Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
//                    List<Map<String, Object>> f = (java.util.List<Map<String, Object>>) result.get("root");
        Form hi = new Form("Your Catalogues", new BoxLayout(BoxLayout.Y_AXIS));

                    InfiniteScrollAdapter.createInfiniteScroll(hi.getContentPane(), () -> {
                        java.util.List<Map<String, Object>> data = fetchPropertyDataOfUser(id);
                        MultiButton[] cmps = new MultiButton[data.size()];
                        for (int iter = 0; iter < cmps.length; iter++) {
                            Map<String, Object> currentListing = data.get(iter);
                            if (currentListing == null) {
                                InfiniteScrollAdapter.addMoreComponents(hi.getContentPane(), new Component[0], false);
                                return;
                            }
                            String thumb_url = (String) currentListing.get("img_url");
                            String guid = (String) currentListing.get("title");
                            String summary = (String) currentListing.get("description");
                            System.out.println(thumb_url);
                            System.out.println(guid);
                            System.out.println(summary);
                            com.mycompany.entities.Catalogue cat = new com.mycompany.entities.Catalogue();
                            cat.setImg_url(thumb_url);
                            cat.setTitle(guid);
                            cat.setDescription(summary);

                            cmps[iter] = new MultiButton(summary);
                            cmps[iter].setIcon(URLImage.createToStorage(placeholder, guid, thumb_url));
                            cmps[iter].addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent ev) {

                                    Catalogue cat_form = new Catalogue(cat);
                                    cat_form.showForm();

                                }

                            });
                        }
                        InfiniteScrollAdapter.addMoreComponents(hi.getContentPane(), cmps, true);
                    }, true);
                    hi.show();

                    System.out.println("Showing");
               
            }
        };
        hi.getToolbar().addCommandToOverflowMenu(show);
        
              Command add = new Command("Show Mine", im) {

            @Override
            public void actionPerformed(ActionEvent evt) {
            

                Form hi = new Form("add catalogue", new BoxLayout(BoxLayout.Y_AXIS));


                hi.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
                    hi.show();
        TextField txt_Title = new TextField();
        txt_Title.setHint("Title");
        hi.addComponent(txt_Title);  
        TextField txt_Description = new TextField();
        txt_Description.setHint("Description");
        hi.addComponent(txt_Description);
        Button btn_add = new Button("add");
        btn_add.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        
                                    ConnectionRequest r = new ConnectionRequest();
            r.setPost(false);
            r.setUrl("http://127.0.0.1:8000/userpost/?title="+txt_Title.getText()+"&description="+txt_Description.getText()+"&user="+id);

            NetworkManager.getInstance().addToQueueAndWait(r);
                        

                    }
                });
        

                    System.out.println("Showing");
               
            }
        };  
        

        InfiniteScrollAdapter.createInfiniteScroll(hi.getContentPane(), () -> {
            java.util.List<Map<String, Object>> data = fetchPropertyData("Leeds");
            MultiButton[] cmps = new MultiButton[data.size()];
            for (int iter = 0; iter < cmps.length; iter++) {
                Map<String, Object> currentListing = data.get(iter);
                if (currentListing == null) {
                    InfiniteScrollAdapter.addMoreComponents(hi.getContentPane(), new Component[0], false);
                    return;
                }
                String thumb_url = (String) currentListing.get("img_url");
                String guid = (String) currentListing.get("title");
                String summary = (String) currentListing.get("description");
                System.out.println(thumb_url);
                System.out.println(guid);
                System.out.println(summary);
                com.mycompany.entities.Catalogue cat = new com.mycompany.entities.Catalogue();
                cat.setImg_url(thumb_url);
                cat.setTitle(guid);
                cat.setDescription(summary);

                cmps[iter] = new MultiButton(summary);
                cmps[iter].setIcon(URLImage.createToStorage(placeholder, guid, thumb_url));
                cmps[iter].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ev) {

                        Catalogue cat_form = new Catalogue(cat);
                        cat_form.showForm();

                    }

                });
            }
            InfiniteScrollAdapter.addMoreComponents(hi.getContentPane(), cmps, true);
        }, true);
        hi.show();
    }
    int pageNumber = 1;

    java.util.List<Map<String, Object>> fetchPropertyData(String text ) {
        try {
            ConnectionRequest r = new ConnectionRequest();
            r.setPost(false);
            r.setUrl("http://127.0.0.1:8000/catalogues");

            NetworkManager.getInstance().addToQueueAndWait(r);
            Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
            return (java.util.List<Map<String, Object>>) result.get("root");
        } catch (Exception err) {
            Log.e(err);
            return null;
        }
    }
    java.util.List<Map<String, Object>> fetchPropertyDataOfUser(String text ) {
        try {
            ConnectionRequest r = new ConnectionRequest();
            r.setPost(false);
            r.setUrl("http://127.0.0.1:8000/catalogues/"+text);

            NetworkManager.getInstance().addToQueueAndWait(r);
            Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
            return (java.util.List<Map<String, Object>>) result.get("root");
        } catch (Exception err) {
            Log.e(err);
            return null;
        }
    }

//-- DON'T EDIT BELOW THIS LINE!!!


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.FlowLayout());
        setTitle("Home");
        setName("Home");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
}
