package com.pidev.forms;

import Utils.Logins;
import com.codename1.db.Database;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.social.FacebookConnect;
import com.codename1.social.Login;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Component;
import com.codename1.ui.Dialog;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * GUI builder created Form
 *
 * @author noon3
 */
public class GuiComponent extends com.codename1.ui.Form {

    private static GuiComponent INSTANCE;
    Database db;
    boolean created = false;

    public GuiComponent() throws IOException {
                this(com.codename1.ui.util.Resources.getGlobalResources());

        
                    created = Database.exists("dr.db");
            db = Database.openOrCreate("dr.db");

            if (created == false) {
                db.execute("create table facebook ( email TEXT, username TEXT, ID TEXT);");
            }
        
        
        NetworkManager.getInstance().addErrorListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                //handle your error here consume the event
                evt.consume();
            }
        });
        gui_btn_signup.addActionListener((e) -> {
            System.out.println(e);
            SignUp.getInstance().CleanForm();
            SignUp.getInstance().show();

        });
        gui_btn_login.addActionListener((e) -> {
            String email = gui_tf_email.getText();
            String password = gui_tx_password.getText();
            ConnectionRequest req = new ConnectionRequest() {

                @Override
                protected void handleErrorResponseCode(int code, String message) {
                    System.out.println("haaa");
                    Dialog dlg = new Dialog("Authentication");
                    Style dlgStyle = dlg.getDialogStyle();
                    dlgStyle.setBorder(Border.createEmpty());
                    dlgStyle.setBgTransparency(255);
                    dlgStyle.setBgColor(0xffffff);

                    Label title = dlg.getTitleComponent();
                    title.getUnselectedStyle().setFgColor(0xff);
                    title.getUnselectedStyle().setAlignment(Component.LEFT);

                    dlg.setLayout(BoxLayout.y());
                    Label blueLabel = new Label();
                    blueLabel.setShowEvenIfBlank(true);
                    blueLabel.getUnselectedStyle().setBgColor(0xff);
                    blueLabel.getUnselectedStyle().setPadding(1, 1, 1, 1);
                    blueLabel.getUnselectedStyle().setPaddingUnit(Style.UNIT_TYPE_PIXELS);
                    dlg.add(blueLabel);
                    TextArea ta = new TextArea("Username / Password error");
                    ta.setEditable(false);
                    ta.setUIID("DialogBody");
                    ta.getAllStyles().setFgColor(0);
                    dlg.add(ta);

                    Label grayLabel = new Label();
                    grayLabel.setShowEvenIfBlank(true);
                    grayLabel.getUnselectedStyle().setBgColor(0xcccccc);
                    grayLabel.getUnselectedStyle().setPadding(1, 1, 1, 1);
                    grayLabel.getUnselectedStyle().setPaddingUnit(Style.UNIT_TYPE_PIXELS);
                    dlg.add(grayLabel);

                    Button ok = new Button(new Command("OK"));
                    ok.getAllStyles().setBorder(Border.createEmpty());
                    ok.getAllStyles().setFgColor(0);
                    dlg.add(ok);
                    dlg.showDialog();
                    kill();
                }

            };
            req.setPost(true);
            req.setUrl("http://localhost:8000/app_dev.php/oauth/v2/token");
            req.addArgument("grant_type", "password");
            req.addArgument("client_id", "1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4");
            req.addArgument("client_secret", "4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k");
            req.addArgument("username", email);
            req.addArgument("password", password);
            NetworkManager.getInstance().addToQueue(req);
            req.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {

                    System.out.println("innnnnnnn");
                    byte[] data = (byte[]) evt.getMetaData();
                    String s = new String(data);
                    try {
                        Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
                        ConnectionRequest reqq = new ConnectionRequest();
                        reqq.setPost(false);
                        reqq.setUrl("http://localhost:8000/app_dev.php/token/" + result.get("access_token"));
                        NetworkManager.getInstance().addToQueue(reqq);
                        reqq.addResponseListener(new ActionListener<NetworkEvent>() {

                            @Override
                            public void actionPerformed(NetworkEvent evt) {
                                byte[] data = (byte[]) evt.getMetaData();
                                String s = new String(data);
                                try {
                                    Map<String, Object> results = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
                                    java.util.List root;
                                    root = (java.util.List) results.get("root");
                                    Map<String, String> resultss;
                                    results = (Map<String, Object>) root.get(0);
                                    Map<String, String> user = (Map<String, String>) results.get("user");
                                    System.out.println(user.get("username"));
                                    Home home = new Home(String.valueOf(user.get("id")), user.get("username"));
                                    home.showForm();

                                } catch (UnsupportedEncodingException ex) {

                                } catch (IOException ex) {

                                }

                            }
                        });

                        System.out.println(result.keySet());
                        System.out.println(result.values());

                    } catch (IOException ex) {

                    }
                }
            });

        });

        gui_btn_fb.addActionListener((e) -> {
            String clientId = "1882681225314626";
            String redirectURI = "http://localhost/hello.php";
            String clientSecret = "e1356afb4fd555113eaa367df668883d";
            FacebookConnect fb = FacebookConnect.getInstance();
            fb.setClientId(clientId);
            fb.setRedirectURI(redirectURI);
            fb.setClientSecret(clientSecret);
Logins.UserData data = new Logins.FacebookData() ;
            doLogin(fb, new Logins.FacebookData());
//Sets a LoginCallback listener
            fb.setCallback(new LoginCallback() {
                @Override
                public void loginSuccessful() {

                    System.out.println("success");
                    System.out.println(fb.getAccessToken().getToken());
                    Logins.UserData data = new Logins.FacebookData() ;
                    
                              data.fetchData(fb.getAccessToken().getToken(), () -> {
                            // we store the values of result into local variables
                            
                            // we then store the data into local cached storage so they will be around when we run the app next time
                            Preferences.set("email", data.getName());
                            Preferences.set("uniqueId", data.getId());
                            Preferences.set("imageURL", data.getImage());
                            Preferences.set("token", fb.getAccessToken().getToken());
                            
                            
                             ConnectionRequest reqq = new ConnectionRequest();
                        reqq.setPost(false);
                        reqq.setUrl("http://127.0.0.1:8000/email/" + data.getName());
                        NetworkManager.getInstance().addToQueue(reqq);
                        reqq.addResponseListener(new ActionListener<NetworkEvent>() {

                            @Override
                            public void actionPerformed(NetworkEvent evt) {
                                byte[] data = (byte[]) evt.getMetaData();
                                String s = new String(data);
                                try {
                                    Map<String, Object> results = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(data), "UTF-8"));
                                    java.util.List root;
                                    root = (java.util.List) results.get("root");
                                    Map<String, String> resultss;
                                    results = (Map<String, Object>) root.get(0);
                                     String userid = (String.valueOf( results.get("id")));
                                     String username = (String) results.get("username");
                                     String email = (String) results.get("email");
                                    System.out.println(results);
                                    System.out.println(userid);
                                    db.execute("insert into facebook values ('"+email+"','"+username+"','"+userid+"');");
                                    System.out.println("insered");
                                    Home home = new Home(userid, username);
                                    home.showForm();

                                } catch (UnsupportedEncodingException ex) {

                                } catch (IOException ex) {

                                }

                            }
                        });


                            
                            
                            
                            
                            // token expiration is in seconds from the current time, we convert it to a System.currentTimeMillis value so we can
                            // reference it in the future to check expiration
                            Preferences.set("tokenExpires", Logins.tokenExpirationInMillis(fb.getAccessToken()));
                    

                });
                    
                    
                    
                    
        

                }

                @Override
                public void loginFailed(String errorMessage) {
                    Dialog.show("No!", "it did not work!", "sad", null);
                }
            });

            // button was clicked, you can do anything you want here...
            if (!fb.isUserLoggedIn()) {

                fb.doLogin();
                System.out.println("I'm here ");
            } else {
                //get the token and now you can query the facebook API
                String token = fb.getAccessToken().getToken();
                System.out.println("logged w omouri wedh7a ");
                System.out.println(token);
            }

            // button was clicked, you can do anything you want here...


        });

        INSTANCE = this;

    }

    public GuiComponent(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

    private void guiBuilderBindComponentListeners() {
        EventCallbackClass callback = new EventCallbackClass();
        gui_btn_login.addActionListener(callback);
        gui_btn_signup.addActionListener(callback);
    }

    class EventCallbackClass implements com.codename1.ui.events.ActionListener, com.codename1.ui.events.DataChangedListener {

        private com.codename1.ui.Component cmp;

        public EventCallbackClass(com.codename1.ui.Component cmp) {
            this.cmp = cmp;
        }

        public EventCallbackClass() {
        }

        public void actionPerformed(com.codename1.ui.events.ActionEvent ev) {
            com.codename1.ui.Component sourceComponent = ev.getComponent();

            if (sourceComponent.getParent().getLeadParent() != null && (sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.MultiButton || sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.SpanButton)) {
                sourceComponent = sourceComponent.getParent().getLeadParent();
            }

            if (sourceComponent == gui_btn_login) {
                onbtn_loginActionEvent(ev);
            }
            if (sourceComponent == gui_btn_signup) {
                onbtn_signupActionEvent(ev);
            }
        }

        public void dataChanged(int type, int index) {
        }
    }

//-- DON'T EDIT BELOW THIS LINE!!!
    private com.codename1.ui.Container gui_Container_3 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    private com.codename1.ui.Container gui_Container_1_1 = new com.codename1.ui.Container(new com.codename1.ui.table.TableLayout(4, 4));
    private com.codename1.ui.Label gui_Label_1_1_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_email = new com.codename1.ui.TextField();
    private com.codename1.ui.Container gui_Container_2_1 = new com.codename1.ui.Container(new com.codename1.ui.table.TableLayout(4, 4));
    private com.codename1.ui.Label gui_Label_2_1_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tx_password = new com.codename1.ui.TextField();
    private com.codename1.ui.Container gui_Container_1_2 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    private com.codename1.ui.Container gui_Container_1_2_2 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    private com.codename1.ui.Button gui_btn_login = new com.codename1.ui.Button();
    private com.codename1.ui.Button gui_btn_signup = new com.codename1.ui.Button();
    private com.codename1.ui.Button gui_btn_fb = new com.codename1.ui.Button();

// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
        setTitle("Hello");
        setName("GuiComponent");
        addComponent(gui_Container_3);

        gui_Container_3.setName("Container_3");
        gui_Container_3.addComponent(gui_Container_1_1);
        gui_Container_1_1.setName("Container_1_1");
        com.codename1.ui.table.TableLayout.Constraint Label_1_1_1Constraint = ((com.codename1.ui.table.TableLayout) gui_Container_1_1.getLayout()).createConstraint(-1, -1);
        gui_Container_1_1.addComponent(Label_1_1_1Constraint, gui_Label_1_1_1);
        com.codename1.ui.table.TableLayout.Constraint tf_emailConstraint = ((com.codename1.ui.table.TableLayout) gui_Container_1_1.getLayout()).createConstraint(-1, -1);
        gui_Container_1_1.addComponent(tf_emailConstraint, gui_tf_email);
        gui_Label_1_1_1.setText("Email :");
        gui_Label_1_1_1.setName("Label_1_1_1");
        gui_tf_email.setName("tf_email");
        gui_Container_3.addComponent(gui_Container_2_1);
        gui_Container_2_1.setName("Container_2_1");
        com.codename1.ui.table.TableLayout.Constraint Label_2_1_1Constraint = ((com.codename1.ui.table.TableLayout) gui_Container_2_1.getLayout()).createConstraint(-1, -1);
        gui_Container_2_1.addComponent(Label_2_1_1Constraint, gui_Label_2_1_1);
        com.codename1.ui.table.TableLayout.Constraint tx_passwordConstraint = ((com.codename1.ui.table.TableLayout) gui_Container_2_1.getLayout()).createConstraint(-1, -1);
        gui_Container_2_1.addComponent(tx_passwordConstraint, gui_tx_password);
        gui_Label_2_1_1.setText("Password :");
        gui_Label_2_1_1.setName("Label_2_1_1");
        gui_tx_password.setName("tx_password");
        gui_Container_3.addComponent(gui_Container_1_2);
        gui_Container_1_2.setName("Container_1_2");
        gui_Container_1_2.addComponent(gui_btn_login);
        gui_Container_1_2.addComponent(gui_btn_signup);
        gui_btn_login.setText("Login");
        gui_btn_login.setName("btn_login");
        gui_btn_signup.setText("Sign Up");
        gui_btn_signup.setName("btn_signup");
        gui_Container_1_2_2.setName("Container_1_2_2");
        gui_Container_1_2_2.addComponent(gui_btn_fb);
        addComponent(gui_Container_1_2_2);

        gui_btn_fb.setText("Facebook login");
        gui_btn_fb.setName("btn_fb");
        gui_Container_1_1.setName("Container_1_1");
        gui_Container_2_1.setName("Container_2_1");
        gui_Container_1_2.setName("Container_1_2");
        gui_Container_3.setName("Container_3");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
    public void onbtn_loginActionEvent(com.codename1.ui.events.ActionEvent ev) {

    }

    public void onbtn_signupActionEvent(com.codename1.ui.events.ActionEvent ev) {
        SignUp.getInstance().showBack();
    }

    public static GuiComponent getInstance() throws IOException {
        if (INSTANCE == (null)) {
            System.out.println("pass");
            INSTANCE = new GuiComponent();
            return INSTANCE;
        } else {
            System.out.println("ssap");
            return INSTANCE;
        }

    }

    public void doLogin(Login lg, Logins.UserData data) {
        if (lg.isUserLoggedIn()) {
            System.out.println("user is logged in");

            return;
        }


        lg.setCallback(new LoginCallback() {
            @Override
            public void loginFailed(String errorMessage) {
                Dialog.show("Error Logging In", "There was an error logging in: " + errorMessage, "OK", null);
            }

            @Override
            public void loginSuccessful() {
                System.out.println("gtting data");
                // when login is successful we fetch the full data
                data.fetchData(lg.getAccessToken().getToken(), () -> {
                    // we store the values of result into local variables

                    // we then store the data into local cached storage so they will be around when we run the app next time
                    Preferences.set("fullName", data.getName());
                    Preferences.set("uniqueId", data.getId());
                    Preferences.set("imageURL", data.getImage());
                    Preferences.set("token", lg.getAccessToken().getToken());

                    // token expiration is in seconds from the current time, we convert it to a System.currentTimeMillis value so we can
                    // reference it in the future to check expiration
                    Preferences.set("tokenExpires", Logins.tokenExpirationInMillis(lg.getAccessToken()));

                });
            }
        });
        lg.doLogin();

    }

}
