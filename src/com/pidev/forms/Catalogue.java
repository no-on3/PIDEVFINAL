/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pidev.forms;

import Utils.Logins;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.AccessToken;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Preferences;
import com.codename1.social.FacebookConnect;
import com.codename1.social.Login;
import com.codename1.social.LoginCallback;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.entities.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * GUI builder created Form
 *
 * @author noon3
 */
public class Catalogue extends com.codename1.ui.Form {

    com.mycompany.entities.Catalogue C;
    private Resources theme;

    public Catalogue() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }

    public Catalogue(com.mycompany.entities.Catalogue C) {
        this(com.codename1.ui.util.Resources.getGlobalResources());
        this.C = C;
        theme = Resources.getGlobalResources();

    }

    public Catalogue(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.FlowLayout());
        setTitle("Catalogue");
        setName("Catalogue");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
    public void showForm() {
        Form hi = new Form(this.C.getTitle(), new BoxLayout(BoxLayout.Y_AXIS));

        ImageViewer imgCon = new ImageViewer();
        EncodedImage palceHolder = EncodedImage.createFromImage(theme.getImage("round.png").scaled(250, 250), false);
        URLImage urlImage = URLImage.createToStorage(palceHolder, this.C.getImg_url(), "http://" + this.C.getImg_url());
        imgCon.setImage(urlImage);
        Button share = new Button("share");
        share.addActionListener((e) -> {
            String clientId = "1882681225314626";
            String redirectURI = "http://localhost/hello.php";
            String clientSecret = "e1356afb4fd555113eaa367df668883d";
           // Login fb = FacebookConnect.getInstance();
            // fb.setClientId(clientId);
            // fb.setRedirectURI(redirectURI);
            // fb.setClientSecret(clientSecret);
                        FacebookConnect fb = FacebookConnect.getInstance();
                        fb.setClientId(clientId);
                        fb.setRedirectURI(redirectURI);
                        fb.setClientSecret(clientSecret);
                        
                        doLogin(fb, new Logins.FacebookData(),theme);
//Sets a LoginCallback listener
                        fb.setCallback(new LoginCallback() {
                            @Override
                            public void loginSuccessful() {
                                

                                System.out.println("success");
                                System.out.println(fb.getAccessToken().getToken());
                
                    ConnectionRequest req = new ConnectionRequest() {
                        @Override
                        protected void readResponse(InputStream input) throws IOException {
                            JSONParser parser = new JSONParser();
                            Map<String, Object> parsed = parser.parseJSON(new InputStreamReader(input, "UTF-8"));
                            System.out.println("reading response");
                            System.out.println(parsed);

                        }

                    };

                    req.setPost(true);
                    req.setUrl("https://graph.facebook.com/v2.9/me/feed");
                    req.addArgument("message", C.getTitle() + "\n" + C.getDescription());
                    req.addArgumentNoEncoding("access_token", fb.getAccessToken().getToken());
                    System.out.println("finishing paramtre");

                                req.addResponseListener(event -> {
                                   Log.p(new String(req.getResponseData()));
                                });
                                NetworkManager.getInstance().addToQueue(req);
                            }

                            @Override
                            public void loginFailed(String errorMessage) {
                                Dialog.show("No!", "it did not work!", "sad", null);
                            }
                        });

                        // button was clicked, you can do anything you want here...
                        if (!fb.isUserLoggedIn()) {
                            
                            fb.doLogin();
                            System.out.println("I'm here ");
                        } else {
                            //get the token and now you can query the facebook API
                            String token = fb.getAccessToken().getToken();
                            System.out.println("logged w omouri wedh7a ");
                            System.out.println(token);
                        }
            

            // button was clicked, you can do anything you want here...
            if( !fb.isUserLoggedIn()) {
                System.out.println("am not logged");


            } else {
             System.out.println("I'm here  and I'm logged");

                //get the token and now you can query the facebook API
                System.out.println("akher if ");
                System.out.println(fb.getAccessToken());

            }

        });

        SpanLabel spDesc = new SpanLabel();
        spDesc.setText(this.C.getDescription());
        hi.add(imgCon);
        hi.add(spDesc);
        hi.add(share);
        hi.show();

    }

    public void doLogin(Login lg, Logins.UserData data, Resources theme) {
        if (lg.isUserLoggedIn()) {
            System.out.println("user is logged in");

            return;
        }

        // if the user already logged in previously and we have a token
        String t = Preferences.get("token", (String) null);
        if (t != null) {
            // we check the expiration of the token which we previously stored as System time
            long tokenExpires = Preferences.get("tokenExpires", (long) -1);
            if (tokenExpires < 0 || tokenExpires > System.currentTimeMillis()) {
                // we are still logged in
                Home home = new Home();
                home.showForm();
                return;
            }
        }
        System.out.println("behi hani hne");
        lg.setCallback(new LoginCallback() {
            @Override
            public void loginFailed(String errorMessage) {
                Dialog.show("Error Logging In", "There was an error logging in: " + errorMessage, "OK", null);
            }

            @Override
            public void loginSuccessful() {
                // when login is successful we fetch the full data
                data.fetchData(lg.getAccessToken().getToken(), () -> {
                    // we store the values of result into local variables
                    uniqueId = data.getId();
                    fullName = data.getName();
                    imageURL = data.getImage();

                    // we then store the data into local cached storage so they will be around when we run the app next time
                    Preferences.set("fullName", fullName);
                    Preferences.set("uniqueId", uniqueId);
                    Preferences.set("imageURL", imageURL);
                    Preferences.set("token", lg.getAccessToken().getToken());

                    // token expiration is in seconds from the current time, we convert it to a System.currentTimeMillis value so we can
                    // reference it in the future to check expiration
                    Preferences.set("tokenExpires", Logins.tokenExpirationInMillis(lg.getAccessToken()));
                Home home = new Home();
                home.showForm();
                });
            }
        });
        System.out.println("hne zeda");
        lg.doLogin();
                System.out.println("w ghadi");

    }
    private String fullName;
    private String uniqueId;
    private String imageURL;
    /**
     * token expiration is in seconds from the current time, we convert it to a
     * System.currentTimeMillis value so we can reference it in the future to
     * check expiration
     */
}
