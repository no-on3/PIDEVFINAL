/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pidev.forms;

/**
 * GUI builder created Form
 *
 * @author noon3
 */
import com.codename1.ui.Display;
import java.io.IOException;


public class SignUp extends com.codename1.ui.Form {
    private static SignUp INSTANCE;

    public SignUp() {
        this(com.codename1.ui.util.Resources.getGlobalResources());
                INSTANCE = this;

    }
    
    public SignUp(com.codename1.ui.util.Resources resourceObjectInstance) {
        initGuiBuilderComponents(resourceObjectInstance);
    }

//-- DON'T EDIT BELOW THIS LINE!!!
    private com.codename1.ui.Container gui_Container_2 = new com.codename1.ui.Container(new com.codename1.ui.table.TableLayout(5, 2));
    private com.codename1.ui.Label gui_Label_1_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_fname = new com.codename1.ui.TextField();
    private com.codename1.ui.Label gui_Label_2_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_lname = new com.codename1.ui.TextField();
    private com.codename1.ui.Label gui_Label_1_2 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_email = new com.codename1.ui.TextField();
    private com.codename1.ui.Label gui_Label_1_2_1_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_pass = new com.codename1.ui.TextField();
    private com.codename1.ui.Label gui_Label_1_1_1_1 = new com.codename1.ui.Label();
    private com.codename1.ui.TextField gui_tf_confpass = new com.codename1.ui.TextField();
    private com.codename1.ui.Container gui_Container_1_1 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    private com.codename1.ui.Button gui_Btn_signup = new com.codename1.ui.Button();
    private com.codename1.ui.Button gui_btn_cancel = new com.codename1.ui.Button();


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void guiBuilderBindComponentListeners() {
        EventCallbackClass callback = new EventCallbackClass();
        gui_Btn_signup.addActionListener(callback);
        gui_btn_cancel.addActionListener(callback);
    }

    class EventCallbackClass implements com.codename1.ui.events.ActionListener, com.codename1.ui.events.DataChangedListener {
        private com.codename1.ui.Component cmp;
        public EventCallbackClass(com.codename1.ui.Component cmp) {
            this.cmp = cmp;
        }

        public EventCallbackClass() {
        }

        public void actionPerformed(com.codename1.ui.events.ActionEvent ev) {
            com.codename1.ui.Component sourceComponent = ev.getComponent();

            if(sourceComponent.getParent().getLeadParent() != null && (sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.MultiButton || sourceComponent.getParent().getLeadParent() instanceof com.codename1.components.SpanButton)) {
                sourceComponent = sourceComponent.getParent().getLeadParent();
            }

            if(sourceComponent == gui_Btn_signup) {
                onBtn_signupActionEvent(ev);
            }
            if(sourceComponent == gui_btn_cancel) {
                try {
                    onbtn_cancelActionEvent(ev);
                } catch (IOException ex) {

                }
            }
        }

        public void dataChanged(int type, int index) {
        }
    }
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        guiBuilderBindComponentListeners();
        setLayout(new com.codename1.ui.table.TableLayout(2, 1));
        setTitle("SignUp");
        setName("SignUp");
        com.codename1.ui.table.TableLayout.Constraint Container_2Constraint = ((com.codename1.ui.table.TableLayout)getLayout()).createConstraint(-1, -1);
        addComponent(Container_2Constraint, gui_Container_2);
        gui_Container_2.setName("Container_2");
        com.codename1.ui.table.TableLayout.Constraint Label_1_1Constraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(Label_1_1Constraint, gui_Label_1_1);
        com.codename1.ui.table.TableLayout.Constraint tf_fnameConstraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(tf_fnameConstraint, gui_tf_fname);
        com.codename1.ui.table.TableLayout.Constraint Label_2_1Constraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(Label_2_1Constraint, gui_Label_2_1);
        com.codename1.ui.table.TableLayout.Constraint tf_lnameConstraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(tf_lnameConstraint, gui_tf_lname);
        com.codename1.ui.table.TableLayout.Constraint Label_1_2Constraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(Label_1_2Constraint, gui_Label_1_2);
        com.codename1.ui.table.TableLayout.Constraint tf_emailConstraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(tf_emailConstraint, gui_tf_email);
        com.codename1.ui.table.TableLayout.Constraint Label_1_2_1_1Constraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(Label_1_2_1_1Constraint, gui_Label_1_2_1_1);
        com.codename1.ui.table.TableLayout.Constraint tf_passConstraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(tf_passConstraint, gui_tf_pass);
        com.codename1.ui.table.TableLayout.Constraint Label_1_1_1_1Constraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(Label_1_1_1_1Constraint, gui_Label_1_1_1_1);
        com.codename1.ui.table.TableLayout.Constraint tf_confpassConstraint = ((com.codename1.ui.table.TableLayout)gui_Container_2.getLayout()).createConstraint(-1, -1);
        gui_Container_2.addComponent(tf_confpassConstraint, gui_tf_confpass);
        gui_Label_1_1.setText("First Name :");
        gui_Label_1_1.setName("Label_1_1");
        gui_tf_fname.setName("tf_fname");
        gui_Label_2_1.setText("Last Name :");
        gui_Label_2_1.setName("Label_2_1");
        gui_tf_lname.setName("tf_lname");
        gui_Label_1_2.setText("Email :");
        gui_Label_1_2.setName("Label_1_2");
        gui_tf_email.setName("tf_email");
        gui_Label_1_2_1_1.setText("Password :");
        gui_Label_1_2_1_1.setName("Label_1_2_1_1");
        gui_tf_pass.setName("tf_pass");
        gui_Label_1_1_1_1.setText("Confirm password :");
        gui_Label_1_1_1_1.setName("Label_1_1_1_1");
        gui_tf_confpass.setName("tf_confpass");
        com.codename1.ui.table.TableLayout.Constraint Container_1_1Constraint = ((com.codename1.ui.table.TableLayout)getLayout()).createConstraint(-1, -1);
        addComponent(Container_1_1Constraint, gui_Container_1_1);
        gui_Container_1_1.setName("Container_1_1");
        gui_Container_1_1.addComponent(gui_Btn_signup);
        gui_Container_1_1.addComponent(gui_btn_cancel);
        gui_Btn_signup.setText("Signup");
        gui_Btn_signup.setName("Btn_signup");
        gui_btn_cancel.setText("Cancel");
        gui_btn_cancel.setName("btn_cancel");
        gui_Container_2.setName("Container_2");
        gui_Container_1_1.setName("Container_1_1");
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
    public void onBtn_signupActionEvent(com.codename1.ui.events.ActionEvent ev) {

        
    }

    public void onbtn_cancelActionEvent(com.codename1.ui.events.ActionEvent ev) throws IOException {
        GuiComponent.getInstance().show();  
        
    }
    public void CleanForm(){
        gui_tf_fname.setText("");
        gui_tf_lname.setText("");
        gui_tf_email.setText("");
        gui_tf_pass.setText("");
        gui_tf_confpass.setText("");
        
    }

        public static SignUp getInstance(){
            if (INSTANCE == (null))
            {
                System.out.println("pass");
                INSTANCE = new SignUp();
                return INSTANCE;
            }
            else {
                System.out.println("ssap");
                
                
                return INSTANCE;
            }
        
    }
}
